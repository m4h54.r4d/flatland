import torch.nn as nn
import torch.nn.functional as F


class DuelingQNetwork(nn.Module):
    """Dueling Q-network (https://arxiv.org/abs/1511.06581)"""

    def __init__(self, state_size, action_size, hidsize1=128, hidsize2=128, hidsize3=128, hidsize4=128, hidsize5=128):
        super(DuelingQNetwork, self).__init__()

        # value network
        self.fc1_val = nn.Linear(state_size, hidsize1)
        self.fc2_val = nn.BatchNorm1d(hidsize1)
        self.fc3_val = nn.Dropout(0.1)
        self.fc4_val = nn.Linear(hidsize1, hidsize2)
        self.fc5_val = nn.BatchNorm1d(hidsize2)
        self.fc6_val = nn.Dropout(0.1)
        self.fc7_val = nn.Linear(hidsize2, hidsize3)
        self.fc8_val = nn.BatchNorm1d(hidsize3)
        self.fc9_val = nn.Dropout(0.1)
        self.fc10_val = nn.Linear(hidsize3, hidsize4)
        self.fc11_val = nn.BatchNorm1d(hidsize4)
        self.fc12_val = nn.Dropout(0.1)
        self.fc13_val = nn.Linear(hidsize4, hidsize5)
        self.fc14_val = nn.BatchNorm1d(hidsize5)
        self.fc15_val = nn.Dropout(0.1)
        self.fc16_val = nn.Linear(hidsize5, 1)

        # advantage network
        self.fc1_adv = nn.Linear(state_size, hidsize1)
        self.fc2_adv = nn.BatchNorm1d(hidsize1)
        self.fc3_adv = nn.Dropout(0.1)
        self.fc4_adv = nn.Linear(hidsize1, hidsize2)
        self.fc5_adv = nn.BatchNorm1d(hidsize2)
        self.fc6_adv = nn.Dropout(0.1)
        self.fc7_adv = nn.Linear(hidsize2, hidsize3)
        self.fc8_adv = nn.BatchNorm1d(hidsize3)
        self.fc9_adv = nn.Dropout(0.1)
        self.fc10_adv = nn.Linear(hidsize3, hidsize4)
        self.fc11_adv = nn.BatchNorm1d(hidsize4)
        self.fc12_adv = nn.Dropout(0.1)
        self.fc13_adv = nn.Linear(hidsize4, hidsize5)
        self.fc14_adv = nn.BatchNorm1d(hidsize5)
        self.fc15_adv = nn.Dropout(0.1)
        self.fc16_adv = nn.Linear(hidsize5, action_size)

    def forward(self, x):
        val = self.fc1_val(x)
        val = F.leaky_relu(self.fc2_val(val))
        val = self.fc3_val(val)
        val = self.fc4_val(val)
        val = F.leaky_relu(self.fc5_val(val))
        val = self.fc6_val(val)
        val = self.fc7_val(val)
        val = F.leaky_relu(self.fc8_val(val))
        val = self.fc9_val(val)
        val = self.fc10_val(val)
        val = F.leaky_relu(self.fc11_val(val))
        val = self.fc12_val(val)
        val = self.fc13_val(val)
        val = F.leaky_relu(self.fc14_val(val))
        val = self.fc15_val(val)
        val = self.fc16_val(val)

        # advantage calculation
        adv = self.fc1_adv(x)
        adv = F.leaky_relu(self.fc2_adv(adv))
        adv = self.fc3_adv(adv)
        adv = self.fc4_adv(adv)
        adv = F.leaky_relu(self.fc5_adv(adv))
        adv = self.fc6_adv(adv)
        adv = self.fc7_adv(adv)
        adv = F.leaky_relu(self.fc8_adv(adv))
        adv = self.fc9_adv(adv)
        adv = self.fc10_adv(adv)
        adv = F.leaky_relu(self.fc11_adv(adv))
        adv = self.fc12_adv(adv)
        adv = self.fc13_adv(adv)
        adv = F.leaky_relu(self.fc14_adv(adv))
        adv = self.fc15_adv(adv)
        adv = self.fc16_adv(adv)

        return val + adv - adv.mean()
